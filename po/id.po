# Indonesian translation of pipewire
# Copyright (C) 2011 THE pipewire'S COPYRIGHT HOLDER
# This file is distributed under the same license as the pipewire package.
#
# Translators:
# Andika Triwidada <andika@gmail.com>, 2011, 2012, 2018.
msgid ""
msgstr ""
"Project-Id-Version: PipeWire master\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pipewire/pipewire-media-"
"session/issues/new\n"
"POT-Creation-Date: 2021-10-20 10:03+1000\n"
"PO-Revision-Date: 2021-08-11 10:50+0700\n"
"Last-Translator: Andika Triwidada <andika@gmail.com>\n"
"Language-Team: Indonesia <id@li.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n>1;\n"
"X-Generator: Poedit 2.2.1\n"

#: src/alsa-monitor.c:662
msgid "Built-in Audio"
msgstr "Audio Bawaan"

#: src/alsa-monitor.c:666
msgid "Modem"
msgstr "Modem"

#: src/alsa-monitor.c:675
msgid "Unknown device"
msgstr "Perangkat tak dikenal"
